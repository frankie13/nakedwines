package com.nw.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.nw.models.Appointment;
import com.nw.service.AppointmentService;

/**
 * The controller class for appointments. Deals with all appointment
 * functionality.
 * 
 * @author Chris Salt
 *
 */
@Controller
public class Appointments {

    @Autowired
    private AppointmentService appointmentService;

    /**
     * Available to the view as ModelAttribute.
     * 
     * @return A list of all existing Appointment objects.
     */
    @ModelAttribute("allAppointments")
    public List<Appointment> populateAppointments() {
	return this.appointmentService.getAppointments();
    }

    /**
     * Available to the view as ModelAttribute.
     * 
     * @return A brand new Appointment object to be saved by the view. TODO:
     *         Shouldn't spring be taking care of this?
     */
    @ModelAttribute("appointment")
    public Appointment getAppointment() {
	return new Appointment();
    }

    /**
     * Provides an endpoint for a /appointments GET request.
     * 
     * @return String representing appointments view.
     */
    @RequestMapping(value = "/appointments", method = RequestMethod.GET)
    public String appointments() {
	return "appointments";
    }

    /**
     * Provides an endpoint for a /appointments/create POST request.
     * 
     * This method is called automatically by the view requesting
     * /appointments/create with the correct model object.
     * 
     * @param appointment
     *            The appointment object to be saved.
     * @param bindingResult
     *            Holds the result of the data binding, errors etc.
     * @param model
     *            A map representation of the model.
     * @return A string representing a redirect to the appointments view.
     */
    @RequestMapping(value = "/appointments/create", method = RequestMethod.POST)
    public String saveAppointment(final Appointment appointment,
	    final BindingResult bindingResult, final ModelMap model) {
	System.out.println("appts POST");
	if (bindingResult.hasErrors()) {
	    System.out.println("errors");
	    System.out.println(bindingResult.getAllErrors());
	    return "appointments";
	}
	this.appointmentService.add(appointment);
	model.clear();
	return "redirect:/appointments";
    }

    /**
     * Called automatically by the view. Specifies a date format for binding the
     * form date to the Appointment.date field.
     * 
     * @param webDataBinder
     */
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder) {
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	dateFormat.setLenient(false);
	webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(
		dateFormat, true));
    }
}