package com.nw.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nw.models.Appointment;
import com.nw.models.AppointmentsDAO;

/**
 * Exposes methods on AppointmentsDAO so we don't have to use the DAO directly
 * in our controller.
 * 
 * The @Service annotation indicates class is a "Service".
 * 
 * @author Chris Salt
 *
 */
@Service
public class AppointmentService {

    @Autowired
    private AppointmentsDAO appointmentsDAO;

    /**
     * @return A list of all Appointment objects.
     */
    public List<Appointment> getAppointments() {
	return appointmentsDAO.findAll();
    }

    /**
     * @param appointment
     *            A new appointment to add.
     */
    public void add(Appointment appointment) {
	appointmentsDAO.add(appointment);
    }

}
