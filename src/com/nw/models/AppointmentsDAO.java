package com.nw.models;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Data Access Object for Appointment.
 * 
 * The @Transactional annotation lets Spring start and commit/rollback
 * transactions for you, assuming a transaction manager is available. The @Component
 * annotation indicates that an annotated class is a "component". Such classes
 * are considered as candidates for auto-detection when using annotation-based
 * configuration and classpath scanning.
 * 
 * @author Chris Salt
 *
 */
@Transactional
@Component
public class AppointmentsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    /**
     * A convenience method for getting the current session.
     * 
     * @return current Session object.
     */
    public Session session() {
	return sessionFactory.getCurrentSession();
    }

    /**
     * 
     * @return A list of all appointment objects.
     */
    @SuppressWarnings("unchecked")
    public List<Appointment> findAll() {
	return session().createQuery("from appointment").list();
    }

    /**
     * @param appointment
     *            A new appointment to add.
     */
    public void add(final Appointment appointment) {
	this.session().save(appointment);
    }
}
