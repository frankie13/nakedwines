package com.nw.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * The Appointment model.
 * 
 * The @Entity annotation indicates that the JavaBean is a persistent entity.
 * 
 * The @Table annotation explicitly configures which table the entity is mapped
 * to.
 * 
 * @author Chris Salt
 *
 */
@Entity(name = "appointment")
@Table(name = "appointments")
public class Appointment {

    /**
     * The @Id annotation signifies the field is a primary key. The @Column
     * annotation specifies a column name to store this field. Since the names
     * are the same this is optional. The @GeneratedValue annotation allows auto
     * generation of this field.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Min(1)
    private String description;
    @NotNull
    private Date date;

    /* Getters and setters */
    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public Date getDate() {
	return date;
    }

    public void setDate(Date date) {
	this.date = date;
    }

    @Override
    public String toString() {
	return "Appointment [id=" + id + ", description=" + description
		+ ", date=" + date + "]";
    }
}
