DROP DATABASE nw;
DROP TABLE appointments;
CREATE DATABASE nw;
\connect nw;
CREATE TABLE appointments (
    id        		  SERIAL PRIMARY KEY NOT NULL,
    description       varchar(200) NOT NULL CHECK (description <> ''),
    date   date NOT NULL
);

